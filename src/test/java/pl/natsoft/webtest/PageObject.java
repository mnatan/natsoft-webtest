package pl.natsoft.webtest;

import com.codeborne.selenide.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.codeborne.selenide.Selenide.*;

public class PageObject {

    private Map<String, PageObjectDefinition> pageDefinitions = new HashMap<>();
    private String currentPage;
    private Scenario currentScenario;

    public void readPageDefinitions() throws IOException {
        String directoryName = "PageObjects/";
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        URL directory = PageObject.class.getClassLoader().getResource(directoryName);
        if (directory == null) {
            throw new NotFoundException("Could not find Page Objects definition folder: " + directoryName);
        }

        String directoryPath = directory.getPath();
        File[] files = Objects.requireNonNull(new File(directoryPath).listFiles());
        for (File file : files) {
            if (!file.getName().endsWith(".yaml")) {
                continue;
            }
            PageObjectDefinition def = mapper.readValue(file, PageObjectDefinition.class);
            pageDefinitions.put(def.name, def);
        }
    }

    @Before
    public void beforeCucumber(Scenario scenario) {
        currentScenario = scenario;
    }

    public void attachScreenshot() {
        TakesScreenshot driver = (TakesScreenshot) WebDriverRunner.getWebDriver();
        final byte[] screenshot = driver.getScreenshotAs(OutputType.BYTES);
        currentScenario.embed(screenshot, "image/png");
    }

    public By getSelector(String id) {
        PageObjectDefinition definition = pageDefinitions.get(currentPage);
        return definition.getElementSelector(id);
    }

    @Before
    public void beforeClass() throws IOException {
        readPageDefinitions();
        Configuration.headless = true;
        Configuration.browserSize = "1000x800";
    }

    @Given("^I am on \"([^\"]*)\" page$")
    public void iAmOnPage(String pageName) throws Throwable {
        currentPage = pageName;
        String url = pageDefinitions.get(currentPage).url;
        open(url);
    }

    @When("^I type \"([^\"]*)\" into \"([^\"]*)\"$")
    public void input(String text, String element) {
        $(getSelector(element))
                .setValue(text)
                .pressEnter();
    }

    @And("^I click element \"([^\"]*)\"$")
    public void iClickElement(String element) throws Throwable {
        $(getSelector(element))
                .shouldBe(Condition.visible)
                .click();
    }

    @And("^I click button with text \"([^\"]*)\"$")
    public void clickElementWithText(String text) {
        $(By.linkText(text))
                .shouldBe(Condition.visible)
                .click();
    }

    @Then("^At least (\\d+) elements \"([^\"]*)\" should be visible$")
    public void atLeastElementsShouldBeVisible(int quantity, String element) throws Throwable {
        $$(getSelector(element)).
                shouldBe(CollectionCondition.sizeGreaterThanOrEqual(quantity));
    }

    @After
    public void afterCucumber(Scenario scenario) {
        if (scenario.isFailed()){
            attachScreenshot();
        }
    }


    @When("^I scroll (\\d+)px down$")
    public void iScrollPxDown(int y_offset) throws Throwable {
        Selenide.executeJavaScript("scrollTo(0," + y_offset + ");");
    }

    @And("^Scroll to \"([^\"]*)\"$")
    public void scrollTo(String element) throws Throwable {
        $(getSelector(element)).scrollIntoView(true);
    }

    @And("^Take screenshot$")
    public void stepTakeScreenshot() throws Throwable {
        attachScreenshot();
    }
}
