package pl.natsoft.webtest;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidSelectorException;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class PageObjectDefinition {
    public Map<String, PageElementSelector> elements;
    public String url;
    public String name;

    public By getElementSelector(String id) {
        PageElementSelector elementDefinition = elements.get(id);
        try {
            return (By) By.class
                    .getMethod(elementDefinition.type, String.class)
                    .invoke(null, elementDefinition.value);
        } catch (NoSuchMethodException | ClassCastException e) {
            throw new InvalidSelectorException(
                    "Could not recognize selector type: "
                            + elementDefinition.type
                            + " in Page: "
                            + this.name);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }
}
