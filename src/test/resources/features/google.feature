Feature: Google search

  Scenario: Simple "dog" search under graphics
    Given I am on "google" page
    When I type "dog" into "search bar"
    And I click element "images tab"
    Then At least 30 elements "gallery image" should be visible
