Feature: Onet.pl - strona główna

  Scenario: Main Page contains ads
    Given I am on "onet" page
    Then At least 1 elements "gigaboard slot" should be visible

  Scenario: Main Page contains ads
    Given I am on "onet" page
    When I scroll 8000px down
    Then At least 1 elements "strefa ofert" should be visible
    And Scroll to "strefa ofert"
    And Take screenshot
