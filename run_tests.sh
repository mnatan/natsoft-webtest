#!/usr/bin/env bash
set -e

function publish_artifact {
    zip -r target target/cucumber
    DATE_WITH_TIME=`date "+%Y%m%d-%H%M%S"`
    FILENAME="webtest-results-${DATE_WITH_TIME}-$1.zip"
    mv target.zip $FILENAME

    HOST='ftp.drivehq.com'
    REMOTEPATH='/test'

    ftp -n <<END_SCRIPT
verbose
passive
open $HOST
user $FTP_USER $FTP_PASSWORD
cd $REMOTEPATH
bin
put $FILENAME
quit
END_SCRIPT

    if [[ $? -ne 0 ]]; then
        echo 'Could not publish test artifacts.'
    else
        echo 'Published test artifacts.'
    fi

    if [[ $1 == "FAIL" ]]; then
        exit 1
    fi
}


{ # try
    mvn test &&
    publish_artifact "SUCCESS"
} || { # catch
    publish_artifact "FAIL" &&
    false
}